from setuptools import setup

setup(name='vgmltools',
      version='0.1',
      description='Machine Learning Helper Tools',
      url='https://gitlab.com/verigram-commons/vgmltools',
      author='Verigram',
      author_email='chingiz@verigram.kz',
      license='AGPLv3',
      packages=['vgmltools'],
      zip_safe=False)
