from .. import get_project_root


class Routes:

    def __init__(self):
        self.__project_root = get_project_root()

    @property
    def final_train_data(self):
        return ""

    @property
    def final_test_data(self):
        return ""

    @property
    def raw_train_data(self):
        return ""

    @property
    def raw_test_data(self):
        return ""

    @property
    def models(self):
        return ""


class Catalogue:

    def __init__(self):
        self.__img_ext = ['jpg', 'jpeg', 'png']
        self.__lbl_ext = ['txt']

    def subdirs_of_final_train_data_dir(self):
        pass

    def image_files_in_final_train_data_dir(self):
        pass

    def label_files_in_final_train_data_dir(self):
        pass

    def final_train_data_abs_dirs(self):
        pass

    def final_test_data_dirs(self):
        pass

    def raw_train_data_dirs(self):
        pass

    def raw_test_data_dirs(self):
        pass

