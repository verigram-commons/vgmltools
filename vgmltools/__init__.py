
PROJECT_ROOT: str = ""


def init_project_root(abs_path: str):
    """
    Absolute path to a root directory of the ML projstruct where these tools are going to be used.
    The projstruct of the ML projstruct is expected to conform the Verigram LLP standard
    :param abs_path:
    :return:
    """

    global PROJECT_ROOT
    PROJECT_ROOT = abs_path


def get_project_root() -> str:
    global PROJECT_ROOT
    return PROJECT_ROOT
