from typing import List
from enum import Enum


class Entry:
    pass


class Dataset:

    def get_entries(self):
        pass

    def as_tf_dataset(self):
        pass


class ClassesDataset(Dataset):

    def get_classes(self):
        pass


class DatasetType(Enum):
    TRAIN = 'train'
    TEST = 'test'
    EVAL = 'eval'


class DatasetLoader:
    def __init__(self, ds_type: str):
        self.ds_type = DatasetType(ds_type)
        self._catalogue = None
        self._img_reader = None

    def set_project_catalog(self, catalog):
        self._catalogue = catalog

    def set_img_reader(self, reader):
        self._img_reader = reader

    def load_raw(self) -> Dataset:
        pass

    def load_final(self) -> Dataset:
        pass

    def load_final_tfrecords(self):
        pass


class ClassesDatasetLoader(DatasetLoader):
    pass


import unittest
from ..projstruct.directory import Catalogue
from unittest.mock import MagicMock


class TestClassDatasetLoader(unittest.TestCase):

    def test_general(self):
        catalog = Catalogue()
        catalog.final_train_data_abs_dirs = MagicMock(return_value=["train/final/A/1.jpg", "train/B/2.jpg"])

        loader = ClassesDatasetLoader('train')
        loader.set_project_catalog(catalog)
